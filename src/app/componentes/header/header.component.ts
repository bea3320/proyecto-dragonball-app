import { Component, OnInit } from '@angular/core';
import { Header } from './models/header';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
public header: Header;
  constructor() {

  this.header = {
    icon:{
      img:'http://2.bp.blogspot.com/-PumFKqV7ZDA/VXYYNeI0biI/AAAAAAAAAG4/zQzMbizm-KI/s1600/ballz.png',
      name: "Dragon Ball",
    },
    links: ["Introducción", "Galería", "Artículos"],
    };
  }

  ngOnInit(): void {
  }

}
