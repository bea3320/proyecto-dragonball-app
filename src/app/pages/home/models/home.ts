export interface Home {
  intro: Intro;
  gallery: Gallery;
  articles: Articles;
}

export interface Intro{
  img: string;
  description: string;
}

export interface Gallery{
  img: Array<Image>;
}

export interface Articles{
  article: Array <Article>;
}

export interface Image{
  img: string;
  name: string;
}

export interface Article{
  title:string;
  img: Image;
}

