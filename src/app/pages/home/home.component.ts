import { Component, OnInit } from '@angular/core';
import { Home } from './models/home';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public home: Home;
  constructor() {
    this.home = {

      intro:{
          img:"https://media.metrolatam.com/2021/02/03/shenlong-3c56e6a3917ca1899271b4d66c54c2f8-600x400.jpg",
          description:"Dragon Ball nos cuenta la vida de Son Goku, un niño inspirado en la leyenda china del rey mono  que tiene cola de simio, una nube voladora y un bastón mágico y que acompaña a Bulma por el mundo en busca de las Bolas de Dragón: siete esferas capaces de conceder cualquier deseo al juntarlas e invocar al dragón Shenlong. Durante este viaje irá mejorando sus habilidades como luchador y conocerá a los personajes que le acompañarán durante la mayor parte de la serie: Krilin, Yamcha, Ten Shin Han, Muten Roshi o Piccolo. Esta primera parte de la serie se caracterizó por ser muy cómica y desenfadada, objetivo inicial con el que Toriyama crearía Dragon Ball, aunque más tarde diera un giro radical en este sentido.",
      },

      gallery: {
          img: [
            {
              img:"https://hipertextual.com/files/2014/11/dragon-ball-hairstyles-2.jpg",
              name:'La leyenda del dragón',
            },
            {
              img:'https://cr00.epimg.net/radio/imagenes/2020/02/26/entretenimiento/1582741709_730119_1582741931_noticia_normal.jpg',
              name: 'El secreto de la esfera del Dragón'
            },
            {
              img:'https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/media/image/2019/07/dragon-ball-blu-ray.png',
              name: 'Vigila las esferas del dragón!'
            },
            {
              img:'https://i.blogs.es/27c08c/goku-freecomputerdesktopwallpaper_1920/450_1000.jpeg',
              name: 'La Nube dorada de Roshi'
            },
            {
              img:'https://i.blogs.es/04ac6a/2560_3000/450_1000.jpeg',
              name: 'El Dragón ha sido llamado'
            },
            {
              img:'https://areajugones.sport.es/wp-content/uploads/2020/08/dragon-ball-super-z-gt-u-original.jpg',
              name: 'La metamorfosis de Goku'
            },
          ],
        },

        articles:{
        article: [
          {
            title:'Super Sayan Dios',
            img: {
              img:
                'https://i.ytimg.com/vi/IyIVCtL8osA/maxresdefault.jpg',
              name: '',
            },
          },
          {
            title: 'Dragon Ball Z',
            img: {
              img:
                'https://androidguias.com/wp-content/uploads/2020/08/Dragon-Ball-Legends-bis.jpg',
              name: '',

          }

        }

        ],

    },
  };
}
  ngOnInit(): void {
  }

}
