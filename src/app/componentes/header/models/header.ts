import { I18nContext } from "@angular/compiler/src/render3/view/i18n/context";

export interface Header {
  icon:Icon;
  links: Array<string>;
}

export interface Icon {
  img: string;
  name: string;
}
