import { Component, OnInit } from '@angular/core';
import { Footer } from './models/footer';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
public footer: Footer;
  constructor() {

    this.footer= {
    links:["Aviso Legal", "Política de Privacidad", "Política de Cookies", "Copyright"],
  };

}

  ngOnInit(): void {
  }

}
