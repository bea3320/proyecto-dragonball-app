import { Component, Input, OnInit } from '@angular/core';
import { Gallery } from '../models/home';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
@Input() gallery!: Gallery;

public sayan: boolean;
public goku: string;
public guerrero: string;

constructor() {
  this.sayan = true;
  this.goku ="https://media.tenor.com/images/112b496cb6d4d33a88bab8c3b4f6054f/tenor.gif"
  this.guerrero = "https://media.tenor.com/images/0d210d990cd1256a51342d9582ca6f2d/tenor.gif";

}

  ngOnInit(): void {}

  onButtonClick() : void{
    //if (this.goku == true){
      //this.guerrero="https://media.tenor.com/images/112b496cb6d4d33a88bab8c3b4f6054f/tenor.gif"
    //}else{
      //this.guerrero="https://media.tenor.com/images0d210d990cd1256a51342d9582ca6f2d/tenor.gif"
    //}
    this.sayan = !this.sayan;
  }

}


